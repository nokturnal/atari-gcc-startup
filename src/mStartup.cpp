// CPP startup
#include <mint/osbind.h>
#include <new>

extern "C"
{
  void __assert_fail(const char* assertion, const char* file, unsigned int line, const char* function) 
  {
    Cconws("__assert_fail\r\n");
  }

  void __main() 
  {
    Cconws("__main\r\n");
  }
}

void* operator new(unsigned long size) 
{ 
  return Malloc(size); 
}

void* operator new[](unsigned long size) 
{
  Cconws("new[]\r\n");
  return Malloc(size);
}

void operator delete(void* pPtr) 
{ 
  Free(pPtr); 
}

void operator delete[](void* pPtr) 
{
  Cconws("delete[]\r\n");
  Free(pPtr);
}

// ----------------------------------------------------------------------------------------
// This is a dummy class to force a static initialisation and destruction lists
// to be built by gcc.

struct MicroStartupForceStaticCtorDtor 
{
  int m_data;
  MicroStartupForceStaticCtorDtor() {
     Cconws("static create\r\n");
    m_data = 10;
  }

  ~MicroStartupForceStaticCtorDtor() {
    Cconws("static destruct\r\n");
  }
};

MicroStartupForceStaticCtorDtor ctordtor;

// ----------------------------------------------------------------------------------------
// This is required for basic std::list support

#include <list>
namespace std 
{
  namespace __detail 
  {
    void _List_node_base::_M_hook(std::__detail::_List_node_base* __position) 
    {
      this->_M_next = __position;
      this->_M_prev = __position->_M_prev;
      __position->_M_prev->_M_next = this;
      __position->_M_prev = this;
    }

    void _List_node_base::_M_unhook() 
    {
      _List_node_base* const __next_node = this->_M_next;
      _List_node_base* const __prev_node = this->_M_prev;
      __prev_node->_M_next = __next_node;
      __next_node->_M_prev = __prev_node;
    }
  }
}
