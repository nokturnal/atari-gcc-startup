; custom, minimal startup for C code
; (c) 2011, 2016 Mariusz Buras (all), Pawel Goralski (modifications for C)

        include 'm68k_defs.inc'

        ;for m68k target only
        ifd   __VASM        ;check if VASM
            ;
        else
          echo "you're compiling without VASM or VASM version is too old"
        endif

MSGUNSUPPORTED macro \1
        movem.l a0-a2/d0-d2,-(sp)
        move.l  #strLabel\@,-(sp)
        move.w  #$09,-(sp)
        trap    #1
        addq.l  #6,sp
        movem.l (sp)+,a0-a2/d0-d2
        bra.s   fin\@ 
        trap    #0      ;trap #0 isn't used on atari
        strLabel\@: 
                dc.b 'ERROR: \1 not implemented! ',$0d,$0a,$00
fin\@:
        
endm

        TEXT

        XDEF 	___mulsi3
; int __mulsi3 (int a, int b)        
___mulsi3:
        move.w	4(sp), d0	; x0 -> d0
        mulu.w	10(sp), d0	; x0*y1
        move.w	6(sp), d1	; x1 -> d1
        mulu.w	8(sp), d1	; x1*y0

        ifnd    __mcf5200__
        add.w	d1, d0

        else

        add.l	d1, d0
        endif

        swap	d0
        clr.w	d0
        move.w	6(sp), d1	; x1 -> d1
        mulu.w	10(sp), d1	; x1*y1
        add.l	d1, d0
        rts

        XDEF 	___udivsi3
; unsigned int __udivsi3 (unsigned int a, unsigned int b)    
___udivsi3:
        move.l	d2, -(sp)
        move.l	12(sp), d1	; d1 = divisor
        move.l	8(sp), d0	; d0 = dividend

        cmp.l	$10000, d1      ; divisor >= 2 ^ 16 ?
        bcc.b	.L3		; jcc then try next algorithm
        move.l	d0, d2
        clr.w	d2
        swap	d2
        divu	d1, d2          ; high quotient in lower word
        move.w	d2, d0		; save high quotient
        swap	d0
        move.w	10(sp), d2	; get low dividend + high rest
        divu	d1, d2		; low quotient
        move.w	d2, d0
        bra.s	.L6             ;jra

.L3:	move.l	d1, d2		; use d2 as divisor backup
.L4:	lsr.l	#1, d1          ; shift divisor
        lsr.l	#1, d0          ; shift dividend
        cmp.l	$10000, d1      ; still divisor >= 2 ^ 16 ?
        bcc.b	.L4             ; jcc
        divu	d1, d0		; now we have 16 bit divisor
        and.l	$ffff, d0       ; mask out divisor, ignore remainder

;  Multiply the 16 bit tentative quotient with the 32 bit divisor.  Because of
;  the operand ranges, this might give a 33 bit product.  If this product is
;  greater than the dividend, the tentative quotient was too large.
        move.l	d2, d1
        mulu	d0, d1		; low part, 32 bits
        swap	d2
        mulu	d0, d2		; high part, at most 17 bits
        swap	d2		; align high part with low part
        tst.w	d2		; high part 17 bits?
        bne.b	.L5		; jne, if 17 bits, quotient was too large
        add.l	d2, d1		; add parts
        bcs.b	.L5		; jcs, if sum is 33 bits, quotient was too large
        cmp.l	8(sp), d1	; compare the sum with the dividend
        bls.b	.L6		; jls,if sum > dividend, quotient was too large
.L5:
        subq.l	#1, d0          ; adjust quotient

.L6:
        move.l	(sp)+, d2
        rts

        XDEF 	___umodsi3
; unsigned int __umodsi3 (unsigned int a, unsigned int b)        
___umodsi3:
        move.l	8(sp), d1	; d1 = divisor
        move.l	4(sp), d0	; d0 = dividend
        move.l	d1, -(sp)
        move.l	d0, -(sp)
        jsr	___udivsi3
        addq.l	#8, sp
        move.l	8(sp), d1	; d1 = divisor

        ifnd    __mcf5200__
        move.l	d1, -(sp)
        move.l	d0, -(sp)
        jsr	___mulsi3	; d0 = (a/b)*b
        addq.l	#8, sp
        else
        muls.l	d1,d0
        endif

        move.l	4(sp), d1	; d1 = dividend
        sub.l	d0, d1		; d1 = a - (a/b)*b
        move.l	d1, d0
        rts

        XDEF    ___modsi3
; int __modsi3 (int a, int b)        
___modsi3:
        move.l   8(sp), d1     ; d1 = divisor 
        move.l   4(sp), d0     ; d0 = dividend 
        move.l   d1, -(sp)
        move.l   d0, -(sp)
        jsr     ___divsi3
        addq.l   #8, sp
        move.l   8(sp), d1     ; d1 = divisor 
        ifnd    __mcf5200__
        move.l   d1, -(sp)
        move.l   d0, -(sp)
        jsr     ___mulsi3        ; d0 = (a/b)*b 
        addq.l   #8, sp
        else
        muls.l   d1,d0
        endif

        move.l   4(sp), d1      ; d1 = dividend 
        sub.l    d0, d1         ; d1 = a - (a/b)*b 
        move.l   d1, d0
        rts        

        XDEF    ___divsi3
; int __divsi3 (int a, int b)        
___divsi3:
        move.l   d2, -(sp)
        moveq   #1, d2         ; sign of result stored in d2 (=1 or =-1) 
        move.l   12(sp), d1     ; d1 = divisor 
        bpl.s   .L1
        neg.l    d1
        ifnd    __mcf5200__
        neg.b   d2              ; change sign because divisor <0  
        else
        neg.l    d2             ; change sign because divisor <0  
        endif
.L1:
        move.l   8(sp), d0      ; d0 = dividend 
        bpl.s    .L2
        neg.l    d0
        ifnd    __mcf5200__
        neg.b    d2
        else
        neg.l    d2
        endif
.L2:
        move.l  d1, -(sp)
        move.l   d0, -(sp)
        jsr     ___udivsi3           ; divide abs(dividend) by abs(divisor) 
        addq.l  #8, sp

        tst.b   d2
        bpl.s   .L3
        neg.l   d0

.L3:    move.l  (sp)+, d2
        rts

        XDEF ___muldi3
; long __muldi3 (long a, long b)        
 ___muldi3:
        MSGUNSUPPORTED muldi3
       rts       
        
        ;XDEF ___divdi3
; long __divdi3 (long a, long b)        
;___divdi3:
;        MSGUNSUPPORTED divdi3
;       rts        
        
;        XDEF ___moddi3
; long __moddi3 (long a, long b)        
;___moddi3:
;        MSGUNSUPPORTED moddi3
;        rts

;        XDEF ___udivdi3
; unsigned long __udivdi3 (unsigned long a, unsigned long b)        
;___udivdi3:
;        MSGUNSUPPORTED udivdi3
;        rts 

        ;XDEF ___umoddi3
; unsigned long __umoddi3 (unsigned long a, unsigned long b)        
;___umoddi3:
 ;       MSGUNSUPPORTED umoddi3
 ;       rts 

; floating point conversion 

        XDEF ___fixunssfsi
; unsigned int __fixunssfsi (float a)        
___fixunssfsi:
        MSGUNSUPPORTED fixunssfsi
        rts

; unsupported  double/64 bit integers      
        XDEF ___fixunssfdi
; unsigned long __fixunssfdi (float a)        
___fixunssfdi:
        MSGUNSUPPORTED fixunssfdi       
        rts
      
        XDEF ___floatdisf
; float __floatdisf (long i)        
___floatdisf:        
        MSGUNSUPPORTED floatdisf
        rts
      
        XDEF ___floatundisf
; float __floatundisf (unsigned long i)        
___floatundisf:
        MSGUNSUPPORTED floatundisf
        rts

        XDEF ___fixdfdi
; long __fixdfdi (double a)        
___fixdfdi:       
        MSGUNSUPPORTED fixdfdi
        rts
      
        XDEF ___floatundidf
; double __floatundidf (unsigned long i)        
___floatundidf:
        MSGUNSUPPORTED floatundidf       
        rts

        XDEF ___fixunsdfsi
; unsigned int __fixunsdfsi (double a)        
___fixunsdfsi:
        MSGUNSUPPORTED fixunsdfsi       
        rts

        XDEF ___fixsfdi
; long __fixsfdi (float a)        
___fixsfdi:       
        MSGUNSUPPORTED fixsfdi
        rts

        XDEF ___fixunsdfdi
; unsigned long __fixunsdfdi (double a)        
___fixunsdfdi:
        MSGUNSUPPORTED fixunsdfdi        
        rts

        XDEF ___floatdidf
; double __floatdidf (long i)        
___floatdidf:
        MSGUNSUPPORTED floatdidf        
        rts

