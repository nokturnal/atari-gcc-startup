
; custom, minimal startup for C code
;(c) 2011-2021 Mariusz Buras (all),
;(c) 2016-2021 Pawel Goralski (modifications for C, argc / argv support)

BASEPAGE_SIZE     equ   256

; BASEPAGE struct
    p_lowtpa:       rs.l    1   ; Start address of the TPA
    p_hitpa:        rs.l    1   ; First byte after the end of the TPA
    p_tbase:        rs.l    1   ; Start address of the program code
    p_tlen:         rs.l    1   ; Length of the program code
    p_dbase:        rs.l    1   ; Start address of the DATA segment
    p_dlen:         rs.l    1   ; Length of the DATA section
    p_bbase:        rs.l    1   ; Start address of the BSS segment
    p_blen:         rs.l    1   ; Length of the BSS section
    p_dta:          rs.l    1   ; Pointer to the default DTA Warning: Points first to the command line !
    p_parent:       rs.l    1   ; Pointer to the basepage of the calling processes
    p_resrvd0:      rs.l    1   ; Reserved
    p_env:          rs.l    1   ; Address of the environment string
    p_resrvd1:      rs.b    80  ; Reserved
    p_cmdlin:       rs.b    128 ; Command line
                    rsreset

       ifnd    STACK_SIZE
       echo    'Info: STACK_SIZE not defined explicitly. Setting to default 16kiB.'
STACK_SIZE        equ   16*1024
        endif

        ifnd    REDIRECT_OUTPUT_TO_SERIAL
        echo    'Info: REDIRECT_OUTPUT_TO_SERIAL not defined explicitly. Setting to 0/OFF'
REDIRECT_OUTPUT_TO_SERIAL   equ     0
        endif

        xref _main
        xref _startupCrtHookInit
        xref _startupCrtHookCleanup
        
        xdef _start         ; gcc brownelf default entry point
        xdef start          ; default gcc 4.3.4 entry point

        xdef _exit
        xdef ___main        ; needed by gcc 4.3.4
        xdef _handleBreakpoint

        TEXT
_start:
start:
; Compute size of required memory = text segment size + data segment size + bss segment size
; + stack size + base page size (base page size includes stack size)

        sub.l    a6, a6                             ; clear a6 for debuggers
        move.l   4(sp),a5                           ; address to basepage
        move.l   p_tlen(a5),d0                      ; length of text segment
        add.l    p_dlen(a5),d0                      ; length of data segment
        add.l    p_blen(a5),d0                      ; length of bss segment
        add.l    #STACK_SIZE + BASEPAGE_SIZE,d0     ; length of stackpointer + basepage
                                                    ; d0 now contains program size
                                                    ; Setup longword aligned application stack
        move.l   a5,d1                              ; address to basepage
        add.l    d0,d1                              ; end of program
        and.b   #$fc,d1

        lea     basepage,a0
        move.l  a5,(a0)+
        move.l  d1,sp                             ; new stackspace

        move.l  d0,-(sp)                          ; mshrink()
        pea     (a5)                              ; start of the block
        clr.w   -(sp)                             ;
        move.w  #$4a,-(sp)                        ;
        trap    #1                                ;
        lea.l    12(sp),sp                        ;

; scan environment
        move.l  basepage,a3

        move.l	sp, d0
        sub.l   #STACK_SIZE-4, d0
        and.b   #$fc, d0

        move.l  d0, a1
        move.l  a1, a4                             ; this is envp
        move.l  p_env(a3), a2
        move.b	p_cmdlin(a3), d5
        move.b	#'=', d6
        move.l	#'VGRA', d7
        movea.l	sp, a6

.ScanEnvLoop:
        move.l  a2, (a1)+
        movea.l	a2, a5
        tst.b   (a2)+
        beq	.ScanEnvExit
.loop1:
        tst.b   (a2)+
        bne     .loop1

        move.b	(a5)+, -(a6)
        move.b	(a5)+, -(a6)
        move.b	(a5)+, -(a6)
        move.b	(a5)+, -(a6)
        cmp.l	(a6)+, d7		; argv ?
        bne     .ScanEnvLoop
        cmp.b	(a5), d6		; argv= ?
        bne	.ScanEnvLoop
        cmp.b	#127, d5		; commandLength == 127 ?
        bne	.ScanEnvLoop

        ; now we have found extended arguments
        clr.b	-4(a5)
        clr.l	-4(a1)
        move.l	a1, a5          ; this is argv
        clr.l	d3		; this is argc
        move.l	a2, (a1)+

.xArgLoop:
        tst.b	(a2)+
        bne	.xArgLoop
        move.l	a2, (a1)+
        addq.l	#1, d3
        tst.b	(a2)
        bne	.xArgLoop
        move.l	a1, a6		; stack limit
        clr.l	-(a1)

        bra	.clearBSS       ; we dont need to parse basepage s tail

.ScanEnvExit:
        move.l	a1, a6		; stack limit
        clr.l   -(a1)

; scan commandline
        lea     p_cmdlin(a3), a0
        move    #1, d3
        move.b  (a0), d1
        ext.w   d1
        lea     1(a0, d1.w), a1
        clr.b   (a1)
        clr.l   -(sp)                   ; argv[argc] = 0
        bra     .loop2

.testBlank:                             ; testing blank (seperator)
        cmpi.b  #' '+1, (a1)
        bpl.s   .testQuoteMark
        clr.b   (a1)
        tst.b   1(a1)
        beq.s   .testQuoteMark
        pea     1(a1)
        addq    #1, d3
        bra     .loop2

.testQuoteMark:                         ; testing quotation marks
        cmpi.b  #'"', (a1)
        bne     .loop2
        move.b  #0, (a1)

.scanPrev:                              ; scan previous
        subq    #1, a1
        cmpi.b  #'"', (a1)
        dbeq    d1,.scanPrev
        subq	#1, d1
        bmi     .loop3
        pea     1(a1)
        addq    #1, d3
        clr.b   (a1)

.loop2:
        subq.l  #1, a1
        dbf     d1, .testBlank

.loop3:
        tst.b   1(a1)
        beq     .end
        pea     1(a1)
        addq    #1, d3

.end:
        pea     emptyStr
        movea.l sp, a5

.clearBSS:
        movem.l  d3/a4-5,-(sp)                        ;store results for later

; clear BSS segment
        move.l  basepage,a5
        move.l  p_bbase(a5),a0
        move.l  p_blen(a5),d0                       ;length of bss segment

        cmp.l    #0,d0
        beq.s    .skipBSSclear
        moveq    #0,d1
        
.clear:
        move.b    d1,(a0)+
        subq.l    #1,d0
        bne.b    .clear
.skipBSSclear:
        
;########################## redirect output to serial        
        if (REDIRECT_OUTPUT_TO_SERIAL==1)  
        echo    "Redirect default output to serial port enabled."
; redirect output to serial
.redirectToSerial:
        move.w #2,-(sp)
        move.w #1,-(sp)
        move.w #$46,-(sp)
        trap #1
        addq.l #6,sp
        else
        echo    "Redirect default output to serial port disabled."
        endif


        movem.l  (sp)+,d3/a4-5                      ; restore

; execute main
; Parameter passing:
;   <d0.w>  Command line argument count (argc)
;   <a0.l>  Pointer to command line argument pointer array (argv)
;   <a1.l>  Pointer to tos environment string (env) [not used atm]

        move.w  d3, d0
        move.l  a5, a0
        move.l  a4, a1

        and.l   #$0000ffff,d0

        move.l  a0,-(sp)
        move.l  d0,-(sp)

        jsr     _startupCrtHookInit

        jsr    _main

        jsr    _startupCrtHookCleanup
_exit:
        move.w #0,-(sp) ;pterm0
        trap #1
        addq.l #2,sp
        
        clr.w -(sp)
        trap #1

___main:
        rts

_handleBreakpoint:
        move.w  sr,-(sp)
        move.w  #$2700,sr
        
        movem.l   a0-a2/d0-d2,-(sp)
        pea       brkpntMsgStr 
        move.w    #$09,-(sp)     
        trap      #1           
        addq.l    #6,sp
        movem.l   (sp)+,a0-a2/d0-d2
        move.w    (sp)+,sr        
        rte

        DATA
basepage:    dc.l    0
len:         dc.l    0
        align  2
        dc.b   'minimal crt0 v. 1.3 (c) 2011-2021 Mariusz Buras, Pawel Goralski'

        align  2
emptyStr:
        dc.b    $00
        even

brkpntMsgStr:
        dc.b    'Breakpoint / unsupported  builtin function',$0d,$0a,$00
        even

