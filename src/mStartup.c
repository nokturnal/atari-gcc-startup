
#include <mint/osbind.h>
#include <limits.h>
#include <stdbool.h>

void __assert_fail(const char * assertion, const char * file, unsigned int line, const char * function)
{
    (void)Cconws("assertion failed!:\r\n");
    (void)Cconws(assertion); (void)Cconws("\r\n");

    int n = INT_MIN;
    char lineNumStr[16];
    int i = 0;

    bool isNeg = n<0;

    int n1 = isNeg ? -n : n;

    while(n1!=0)
    {
        lineNumStr[i++] = n1%10+'0';
        n1=n1/10;
    }

    if(isNeg)
        lineNumStr[i++] = '-';

    lineNumStr[i] = '\0';

    for(unsigned int t = 0; t < i/2; t++)
    {
        lineNumStr[t] ^= lineNumStr[i-t-1];
        lineNumStr[i-t-1] ^= lineNumStr[t];
        lineNumStr[t] ^= lineNumStr[i-t-1];
    }

    if(n == 0)
    {
        lineNumStr[0] = '0';
        lineNumStr[1] = '\0';
    }

    (void)Cconws(lineNumStr); (void)Cconws(" ");
    (void)Cconws(function); (void)Cconws("\r\n");
}

#if ENABLE_STACK_PROTECTOR
// move it to startup ?
unsigned long __stack_chk_guard;

void __stack_chk_guard_setup(void)
{
  __stack_chk_guard = 0xDEADDAED;
}

void __stack_chk_fail(void)                         
{
    (void)Cconws("Stack check failed\r\n");
}

#endif
