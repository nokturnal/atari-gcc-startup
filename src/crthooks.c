
#include <stdint.h>
#include <mint/osbind.h>

/* assign an exception handler */
static void installExceptionHandler(const uint8_t exceptionNb, void *pFuncPtr)
{
  ((volatile uint32_t *)0x000000)[exceptionNb] = (uint32_t)pFuncPtr;
}

static uint32_t getExceptionHandlerAddress(const uint8_t exceptionNb)
{
  return ((volatile uint32_t *)0x000000)[exceptionNb];
}

extern void handleBreakpoint(void);

uint32_t oldTrap0Handler = 0;

void startupCrtHookInit(void)
{
  //uint32_t usp = Super(0L);
  //oldTrap0Handler = getExceptionHandlerAddress(32);
  //installExceptionHandler(32,(void *)handleBreakpoint);
  //SuperToUser(usp);
}

void startupCrtHookCleanup(void)
{
  //uint32_t usp = Super(0L);
  //installExceptionHandler(32, (void *)oldTrap0Handler);
  //SuperToUser(usp);
}