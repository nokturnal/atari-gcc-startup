#include <stdint.h>
#include <mint/osbind.h>

// ----------------------------------------------------------------------------------------
// Returns the number of leading 0-bits in x, starting at the most significant
// bit position.
// If x is zero, the result is undefined.
int __clzsi2(int v) 
{
  int p = 31;

  if (v & 0xffff0000)
  {
    p -= 16;
    v >>= 16;
  }

  if (v & 0xff00)
  {
    p -= 8;
    v >>= 8;
  }

  if (v & 0xf0)
  {
    p -= 4;
    v >>= 4;
  }

  if (v & 0xc)
  {
    p -= 2;
    v >>= 2;
  }

  if (v & 0x2)
  {
    p -= 1;
    v >>= 1;
  }

  return p;
}

// Returns the number of trailing 0-bits in x, starting at the least significant
// bit position.
// If x is zero, the result is undefined.
int __ctzsi2(unsigned int x) 
{
  int n = 1;

  if ((x & 0x0000FFFF) == 0)
  {
    n = n + 16;
    x = x >> 16;
  }
  
  if ((x & 0x000000FF) == 0)
  {
    n = n + 8;
    x = x >> 8;
  }
  
  if ((x & 0x0000000F) == 0)
  {
    n = n + 4;
    x = x >> 4;
  }
  
  if ((x & 0x00000003) == 0)
  {
    n = n + 2;
    x = x >> 2;
  }
  
  return n - (x & 1);
}

// the following deal with IEEE single-precision numbers 
#define EXCESS		126L
#define SIGNBIT		0x80000000L
#define HIDDEN		(1L << 23L)
#define SIGN(fp)	((fp) & SIGNBIT)
#define EXP(fp)		(((fp) >> 23L) & 0xFF)
#define MANT(fp)	(((fp) & 0x7FFFFFL) | HIDDEN)
#define PACK(s,e,m)	((s) | ((e) << 23L) | (m))

// the following deal with IEEE double-precision numbers 
#define EXCESSD		1022L
#define HIDDEND		(1L << 20L)
#define EXPDBITS	11
#define EXPDMASK	0x7FFL
#define EXPD(fp)	(((fp.l.upper) >> 20L) & 0x7FFL)
#define SIGND(fp)	((fp.l.upper) & SIGNBIT)
#define MANTD(fp)	(((((fp.l.upper) & 0xFFFFF) | HIDDEND) << 10) | (fp.l.lower >> 22))
#define MANTDMASK	0xFFFFFL      // mask of upper part 

union double_long 
{
  double d;
  struct {
      int32_t upper;
      uint32_t lower;
    } l;
};

union float_long 
{
  float f;
  int32_t l;
};

int32_t __unordsf2 (float a, float b)
{
  union float_long fl;

  fl.f = a;
  if (EXP(fl.l) == EXP(~0u) && (MANT(fl.l) & ~HIDDEN) != 0)
    return 1;
  fl.f = b;
  if (EXP(fl.l) == EXP(~0u) && (MANT(fl.l) & ~HIDDEN) != 0)
    return 1;
  return 0;
}

int32_t __unorddf2 (double a, double b)
{
  union double_long dl;

  dl.d = a;
  if (EXPD(dl) == EXPDMASK
      && ((dl.l.upper & MANTDMASK) != 0 || dl.l.lower != 0))
    return 1;
  dl.d = b;
  if (EXPD(dl) == EXPDMASK
      && ((dl.l.upper & MANTDMASK) != 0 || dl.l.lower != 0))
    return 1;
  return 0;
}

// convert unsigned int to double 
// double __floatunsidf (unsigned int i)
double __floatunsidf (uint32_t a1)
{
  uint32_t exp = 32 + EXCESSD;
  union double_long dl;

  if (!a1)
    {
      dl.l.upper = dl.l.lower = 0;
      return dl.d;
    }

  while (a1 < 0x2000000L)
    {
      a1 <<= 4;
      exp -= 4;
    }

  while (a1 < 0x80000000L)
    {
      a1 <<= 1;
      exp--;
    }

  /* pack up and go home */
  dl.l.upper = exp << 20L;
  dl.l.upper |= (a1 >> 11L) & ~HIDDEND;
  dl.l.lower = a1 << 21L;

  return dl.d;
}

// convert int to double 
// double __floatsidf (int i)
double __floatsidf (int32_t a1)
{
  int32_t sign = 0, exp = 31 + EXCESSD;
  union double_long dl;

  if (!a1)
    {
      dl.l.upper = dl.l.lower = 0;
      return dl.d;
    }

  if (a1 < 0)
    {
      sign = SIGNBIT;
      a1 = (int32_t)-(uint32_t)a1;
      if (a1 < 0)
  {
    dl.l.upper = SIGNBIT | ((32 + EXCESSD) << 20L);
    dl.l.lower = 0;
    return dl.d;
        }
    }

  while (a1 < 0x1000000L)
    {
      a1 <<= 4;
      exp -= 4;
    }

  while (a1 < 0x40000000L)
    {
      a1 <<= 1;
      exp--;
    }

  /* pack up and go home */
  dl.l.upper = sign;
  dl.l.upper |= exp << 20L;
  dl.l.upper |= (a1 >> 10L) & ~HIDDEND;
  dl.l.lower = a1 << 22L;

  return dl.d;
}

// convert unsigned int to float 
// float __floatunsisf (unsigned int i)
float __floatunsisf (uint32_t l)
{
  double foo = __floatunsidf (l);
  return foo;
}

// convert int to float 
// float __floatsisf (int i)
float __floatsisf (int32_t l)
{
  double foo = __floatsidf (l);
  return foo;
}

// convert float to double
double __extendsfdf2 (float a1)
{
  register union float_long fl1;
  register union double_long dl;
  register int32_t exp;
  register int32_t mant;

  fl1.f = a1;

  dl.l.upper = SIGN (fl1.l);
  if ((fl1.l & ~SIGNBIT) == 0)
    {
      dl.l.lower = 0;
      return dl.d;
    }

  exp = EXP(fl1.l);
  mant = MANT (fl1.l) & ~HIDDEN;
  if (exp == 0)
    {
      /* Denormal.  */
      exp = 1;
      while (!(mant & HIDDEN))
  {
    mant <<= 1;
    exp--;
  }
      mant &= ~HIDDEN;
    }
  exp = exp - EXCESS + EXCESSD;
  dl.l.upper |= exp << 20;
  dl.l.upper |= mant >> 3;
  dl.l.lower = mant << 29;
  
  return dl.d;
}

// convert double to float 
// truncate a to the narrower mode of their return type, rounding toward zero. 
float __truncdfsf2 (double a1)
{
  register int32_t exp;
  register int32_t mant;
  register union float_long fl;
  register union double_long dl1;
  int32_t sticky; // int16_t?
  int32_t shift; //int16_t

  dl1.d = a1;

  if ((dl1.l.upper & ~SIGNBIT) == 0 && !dl1.l.lower)
    {
      fl.l = SIGND(dl1);
      return fl.f;
    }

  exp = EXPD (dl1) - EXCESSD + EXCESS;

  sticky = dl1.l.lower & ((1 << 22) - 1);
  mant = MANTD (dl1);
  /* shift double mantissa 6 bits so we can round */
  sticky |= mant & ((1 << 6) - 1);
  mant >>= 6;

  /* Check for underflow and denormals.  */
  if (exp <= 0)
    {
      if (exp < -24)
  {
    sticky |= mant;
    mant = 0;
  }
      else
  {
    sticky |= mant & ((1 << (1 - exp)) - 1);
    mant >>= 1 - exp;
  }
      exp = 0;
    }
  
  /* now round */
  shift = 1;
  if ((mant & 1) && (sticky || (mant & 2)))
    {
      int32_t rounding = exp ? 2 : 1;

      mant += 1;

      /* did the round overflow? */
      if (mant >= (HIDDEN << rounding))
  {
    exp++;
    shift = rounding;
  }
    }
  /* shift down */
  mant >>= shift;

  mant &= ~HIDDEN;

  /* pack up and go home */
  fl.l = PACK (SIGND (dl1), exp, mant);
  return (fl.f);
}

// convert double to int 
// int __fixdfsi (double a)
int32_t __fixdfsi (double a1)
{
  register union double_long dl1;
  register int32_t exp;
  register int32_t l;

  dl1.d = a1;

  if (!dl1.l.upper && !dl1.l.lower) 
    return 0;

  exp = EXPD (dl1) - EXCESSD - 31;
  l = MANTD (dl1);

  if (exp > 0) 
    {
      /* Return largest integer.  */
      return SIGND (dl1) ? 0x80000000L : 0x7fffffffL;
    }

  if (exp <= -32)
    return 0;

  /* shift down until exp = 0 */
  if (exp < 0)
    l >>= -exp;

  return (SIGND (dl1) ? -l : l);
}

// convert float to int 
// int __fixsfsi (float a)
int32_t __fixsfsi (float a1)
{
  double foo = a1;
  return __fixdfsi (foo);
}

#define arith64_u64 unsigned long long int
#define arith64_s64 signed long long int
#define arith64_u32 unsigned int
#define arith64_s32 int

typedef union
{
    arith64_u64 u64;
    arith64_s64 s64;
    struct
    {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        arith64_u32 hi; arith64_u32 lo;
#else
        arith64_u32 lo; arith64_u32 hi;
#endif
    } u32;
    struct
    {
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        arith64_s32 hi; arith64_s32 lo;
#else
        arith64_s32 lo; arith64_s32 hi;
#endif
    } s32;
} arith64_word;

// extract hi and lo 32-bit words from 64-bit value
#define arith64_hi(n) (arith64_word){.u64=n}.u32.hi
#define arith64_lo(n) (arith64_word){.u64=n}.u32.lo

// Negate a if b is negative, via invert and increment.
#define arith64_neg(a, b) (((a) ^ ((((arith64_s64)(b)) >= 0) - 1)) + (((arith64_s64)(b)) < 0))
#define arith64_abs(a) arith64_neg(a, a)

// Return the absolute value of a.
// Note LLINT_MIN cannot be negated.
arith64_s64 __absvdi2(arith64_s64 a)
{
    return arith64_abs(a);
}

// Return the result of shifting a left by b bits.
arith64_s64 __ashldi3(arith64_s64 a, int b)
{
    arith64_word w = {.s64 = a};

    b &= 63;

    if (b >= 32)
    {
        w.u32.hi = w.u32.lo << (b - 32);
        w.u32.lo = 0;
    } else if (b)
    {
        w.u32.hi = (w.u32.lo >> (32 - b)) | (w.u32.hi << b);
        w.u32.lo <<= b;
    }
    return w.s64;
}

// Return the result of arithmetically shifting a right by b bits.
arith64_s64 __ashrdi3(arith64_s64 a, int b)
{
    arith64_word w = {.s64 = a};

    b &= 63;

    if (b >= 32)
    {
        w.s32.lo = w.s32.hi >> (b - 32);
        w.s32.hi >>= 31; // 0xFFFFFFFF or 0
    } else if (b)
    {
        w.u32.lo = (w.u32.hi << (32 - b)) | (w.u32.lo >> b);
        w.s32.hi >>= b;
    }
    return w.s64;
}

// These functions return the number of leading 0-bits in a, starting at the
// most significant bit position. If a is zero, the result is undefined.
/*int __clzsi2(arith64_u32 a)
{
    int b, n = 0;
    b = !(a & 0xffff0000) << 4; n += b; a <<= b;
    b = !(a & 0xff000000) << 3; n += b; a <<= b;
    b = !(a & 0xf0000000) << 2; n += b; a <<= b;
    b = !(a & 0xc0000000) << 1; n += b; a <<= b;
    return n + !(a & 0x80000000);
}*/

int __clzdi2(arith64_u64 a)
{
    int b, n = 0;
    b = !(a & 0xffffffff00000000ULL) << 5; n += b; a <<= b;
    b = !(a & 0xffff000000000000ULL) << 4; n += b; a <<= b;
    b = !(a & 0xff00000000000000ULL) << 3; n += b; a <<= b;
    b = !(a & 0xf000000000000000ULL) << 2; n += b; a <<= b;
    b = !(a & 0xc000000000000000ULL) << 1; n += b; a <<= b;
    return n + !(a & 0x8000000000000000ULL);
}

// These functions return the number of trailing 0-bits in a, starting at the
// least significant bit position. If a is zero, the result is undefined.
/*int __ctzsi2(arith64_u32 a)
{
    int b, n = 0;
    b = !(a & 0x0000ffff) << 4; n += b; a >>= b;
    b = !(a & 0x000000ff) << 3; n += b; a >>= b;
    b = !(a & 0x0000000f) << 2; n += b; a >>= b;
    b = !(a & 0x00000003) << 1; n += b; a >>= b;
    return n + !(a & 0x00000001);
}*/

int __ctzdi2(arith64_u64 a)
{
    int b, n = 0;
    b = !(a & 0x00000000ffffffffULL) << 5; n += b; a >>= b;
    b = !(a & 0x000000000000ffffULL) << 4; n += b; a >>= b;
    b = !(a & 0x00000000000000ffULL) << 3; n += b; a >>= b;
    b = !(a & 0x000000000000000fULL) << 2; n += b; a >>= b;
    b = !(a & 0x0000000000000003ULL) << 1; n += b; a >>= b;
    return n + !(a & 0x0000000000000001ULL);
}

// Calculate both the quotient and remainder of the unsigned division of a and
// b. The return value is the quotient, and the remainder is placed in variable
// pointed to by c (if it's not NULL).
arith64_u64 __divmoddi4(arith64_u64 a, arith64_u64 b, arith64_u64 *c)
{
    if (b > a)                                  // divisor > numerator?
    {
        if (c) *c = a;                          // remainder = numerator
        return 0;                               // quotient = 0
    }
    if (!arith64_hi(b))                         // divisor is 32-bit
    {
        if (b == 0)                             // divide by 0
        {
            volatile char x = 0; x = 1 / x;     // force an exception
        }
        if (b == 1)                             // divide by 1
        {
            if (c) *c = 0;                      // remainder = 0
            return a;                           // quotient = numerator
        }
        if (!arith64_hi(a))                     // numerator is also 32-bit
        {
            if (c)                              // use generic 32-bit operators
                *c = arith64_lo(a) % arith64_lo(b);
            return arith64_lo(a) / arith64_lo(b);
        }
    }

    // let's do long division
    char bits = __clzdi2(b) - __clzdi2(a) + 1;  // number of bits to iterate (a and b are non-zero)
    arith64_u64 rem = a >> bits;                   // init remainder
    a <<= 64 - bits;                            // shift numerator to the high bit
    arith64_u64 wrap = 0;                          // start with wrap = 0
    while (bits-- > 0)                          // for each bit
    {
        rem = (rem << 1) | (a >> 63);           // shift numerator MSB to remainder LSB
        a = (a << 1) | (wrap & 1);              // shift out the numerator, shift in wrap
        wrap = ((arith64_s64)(b - rem - 1) >> 63);  // wrap = (b > rem) ? 0 : 0xffffffffffffffff (via sign extension)
        rem -= b & wrap;                        // if (wrap) rem -= b
    }
    if (c) *c = rem;                            // maybe set remainder
    return (a << 1) | (wrap & 1);               // return the quotient
}

// Return the quotient of the signed division of a and b.
arith64_s64 __divdi3(arith64_s64 a, arith64_s64 b)
{
    arith64_u64 q = __divmoddi4(arith64_abs(a), arith64_abs(b), (void *)0);
    return arith64_neg(q, a^b); // negate q if a and b signs are different
}

// Return the index of the least significant 1-bit in a, or the value zero if a
// is zero. The least significant bit is index one.
int __ffsdi2(arith64_u64 a)
{
    return a ? __ctzdi2(a) + 1 : 0;
}

// Return the result of logically shifting a right by b bits.
arith64_u64 __lshrdi3(arith64_u64 a, int b)
{
    arith64_word w = {.u64 = a};

    b &= 63;

    if (b >= 32)
    {
        w.u32.lo = w.u32.hi >> (b - 32);
        w.u32.hi = 0;
    } else if (b)
    {
        w.u32.lo = (w.u32.hi << (32 - b)) | (w.u32.lo >> b);
        w.u32.hi >>= b;
    }
    return w.u64;
}

// Return the remainder of the signed division of a and b.
arith64_s64 __moddi3(arith64_s64 a, arith64_s64 b)
{
    arith64_u64 r;
    __divmoddi4(arith64_abs(a), arith64_abs(b), &r);
    return arith64_neg(r, a); // negate remainder if numerator is negative
}

// Return the number of bits set in a.
int __popcountsi2(arith64_u32 a)
{
    // collect sums into two low bytes
    a = a - ((a >> 1) & 0x55555555);
    a = ((a >> 2) & 0x33333333) + (a & 0x33333333);
    a = (a + (a >> 4)) & 0x0F0F0F0F;
    a = (a + (a >> 16));
    // add the bytes, return bottom 6 bits
    return (a + (a >> 8)) & 63;
}

// Return the number of bits set in a.
int __popcountdi2(arith64_u64 a)
{
    // collect sums into two low bytes
    a = a - ((a >> 1) & 0x5555555555555555ULL);
    a = ((a >> 2) & 0x3333333333333333ULL) + (a & 0x3333333333333333ULL);
    a = (a + (a >> 4)) & 0x0F0F0F0F0F0F0F0FULL;
    a = (a + (a >> 32));
    a = (a + (a >> 16));
    // add the bytes, return bottom 7 bits
    return (a + (a >> 8)) & 127;
}

// Return the quotient of the unsigned division of a and b.
arith64_u64 __udivdi3(arith64_u64 a, arith64_u64 b)
{
    return __divmoddi4(a, b, (void *)0);
}

// Return the remainder of the unsigned division of a and b.
arith64_u64 __umoddi3(arith64_u64 a, arith64_u64 b)
{
    arith64_u64 r;
    __divmoddi4(a, b, &r);
    return r;
}
