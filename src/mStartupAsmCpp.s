
;custom, minimal startup for C++ code
;(c) 2011-2021 Mariusz Buras (all),
;(c) 2016-2021 Pawel Goralski (cleanup, vasm adjustments) 
        
    xdef    ___cxa_pure_virtual
    xdef    _start              ; gcc brownelf default entry point
    xdef    start               ; default gcc 4.3.4 entry point

    xref    ___CTOR_LIST__
    xref    ___DTOR_LIST__
    xdef    __ZSt17__throw_bad_allocv

    xref _main

BASEPAGE_SIZE     equ   256

       ifnd    STACK_SIZE
       echo    'Info: STACK_SIZE not defined explicitly. Setting to default 64kb.'
STACK_SIZE        equ   64*1024
        endif

        TEXT
_start:
start:
        sub.l       a6,a6                              ; clear a6 for debuggers
        move.l      4(sp),a5                           ; address to basepage
        move.l      $0c(a5),d0                         ; length of text segment
        add.l       $14(a5),d0                         ; length of data segment
        add.l       $1c(a5),d0                         ; length of bss segment
        add.l       #STACK_SIZE + BASEPAGE_SIZE,d0     ; length of stackpointer+basepage
        move.l      a5,d1                              ; address to basepage
        add.l       d0,d1                              ; end of program
        and.l       #$fffffff0,d1                      ; align stack
        move.l      d1,sp                              ; new stackspace

        move.l      d0,-(sp)                           ; mshrink()
        move.l      a5,-(sp)
        clr.w       -(sp)
        move.w      #$4a,-(sp)
        trap        #1
        lea.l       12(sp),sp

        ; clear bss segment
.clearBSS:
        move.l      $18(a5),a0
        move.l      $1c(a5),d0                       ; length of bss segment

        cmp.l       #0,d0
        beq.s       .skipBSSclear
        moveq       #0,d1

.clear:
        move.b      d1,(a0)+
        subq.l      #1,d0
        bne.b       .clear
.skipBSSclear:
    
        if (REDIRECT_OUTPUT_TO_SERIAL==1)  
        ; redirect to serial
.redirectToSerial:        
        move.w      #2,-(sp)
        move.w      #1,-(sp)
        move.w      #$46,-(sp)
        trap        #1
        addq.l      #6,sp
        endif

        ; execute static constructors

        lea         ___CTOR_LIST__,a0
        jsr         static
        jsr         _main

exit:    
        lea         ___DTOR_LIST__,a0
        jsr         static
        
        move.w      #1,-(sp)
        trap        #1
        addq.l      #2,sp
        
        clr.w       -(sp)
        trap        #1

static:    
        move.l      (a0)+,d0
l1:
        move.l      (a0)+,a1
        movem.l     d0/a0,-(sp)
        moveq       #0,d0
        jsr         (a1)
        movem.l     (sp)+,d0/a0
        subq.l      #1,d0
        bne.b       l1
        rts

_basepage:    ds.l    1
_len:         ds.l    1

; --------------------------------------------------------------
__ZSt17__throw_bad_allocv:
___cxa_pure_virtual:
        jmp         exit
