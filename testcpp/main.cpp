
#include <osbind.h>
#include <new>

class parent
{
public:
	
	parent(){}
	~parent(){}

	void SayHelloParent()
	{
		Cconws("It's me parent, really...!\r\n");
	}
	virtual void overrideMe()=0; //pure virtual
	
	virtual void KillYourself()
	{
		Cconws("It's me parent, wanna die...!\r\n");
	}
};

class HelloWorld : public parent
{

public:
	HelloWorld(){}
	~HelloWorld(){}
	
	virtual void overrideMe()
	{
		Cconws("I'm overriden!!!\r\n");
	}

	void PrintHello()
	{
		Cconws("Hello world!\r\n");
		HelpMe();
	}
	
	void KillYourself()
	{
		Cconws("It's me child, wanna die...!\r\n");
	}
	
protected:
	void HelpMe()
	{
		Cconws("Protected hello!\r\n");
	}
	
};

int main(void)
{
	HelloWorld *myObject = nullptr;
	
// test of builtin gcc functions
    volatile int64_t s = 1;
    volatile uint64_t u = 1;

    s += s;
    s -= s;
    s *= s;
    s /= s;
    s %= s;
    s >>= 33;
    s <<= 33;

    u += u;
    u -= u;
    u *= u;
    u /= u;
    u %= u;
    u >>= 33;
    u <<= 33;

    volatile int32_t s32 = 1;
    volatile uint32_t u32 = 1;

    s32 += s32;
    s32 -= s32;
    s32 *= s32;
    s32 /= s32;
    s32 %= s32;
    s32 >>= 31;
    s32 <<= 31;

    u32 += u32;
    u32 -= u32;
    u32 *= u32;
    u32 /= u32;
    u32 %= u32;
    u32 >>= 31;
    u32 <<= 31;

    // float/double to int conversion
    float f = 1000.0f;
   
    int32_t i32f = (int32_t)f;
    uint32_t u32f = (uint32_t)f;
    
    f = (float)i32f;
    f = (float)u32f;
    
    // not supported atm (no builtin compiler functions implemented)
    // additionally no fp arithemtic
   
    //int64_t i64f = (int64_t)f;
    //uint64_t u64f = (uint64_t)f;
    
    //f = (float)i64f;
    //f = (float)u64f;

    // double d = 1000000000.0;
    // int32_t i32d = (int32_t)d;
    // uint32_t u32d = (uint32_t)d;
    // int64_t i64d = (int64_t)d;
    // uint64_t u64d = (uint64_t)d;
    
    //d = (double)i32d;
    //d = (double)u32d;
    //d = (double)i64d;
    //d = (double)u64d;
    
	myObject = new(std::nothrow) HelloWorld();

	if(myObject)
	{
		myObject->PrintHello();
		myObject->overrideMe();
		myObject->KillYourself();
	
		delete(myObject);
		myObject = nullptr;
	}
	else
	{
		Cconws("Error: NEW failed..\r\n");
	}
	
 return 0;
}
