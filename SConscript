
# atari gcc minimal startup
# without stdlib for Atari ST/STe/TT/F030/CT6x targets
# (c) 2011-2022 Mariusz Buras / Pawel Goralskl

import sys
import os

# adds set FastRam flag on executable postbuild action
def setFastRamFlags(env, toolchainType, target):
    if(toolchainType != 'GCCBROWNELF'): # TODO fix / patch for brownelf
        env.AddPostAction(target, Action('m68k-atari-mint-flags --mfastram --mfastload --mfastalloc $TARGET'))

# calls external tool (brownout) to convert elf files to tos executables
def convertElfToTos(env, toolchainPrefixPath, toolchainType, buildTypeName, targetName, outputDirPath, outputExtension, generateDriGstSymbols):
    if(toolchainType == 'GCCBROWNELF'):
        outputPath = outputDirPath + targetName  + '.' + outputExtension
        print('Converting {}.elf to TOS binary --> {}...'.format(targetName,outputPath))
        
        symbolGeneration=''
        if( generateDriGstSymbols == 'yes'):
            symbolGeneration=' -x'
        
        env.AddPostAction(targetName +'.elf', Action(toolchainPrefixPath +'/bin/brownout' + symbolGeneration +' -i $TARGET -o ' + outputPath))

# returns git revision
def getVersion(env):
    git = env.WhereIs('git')
    if git:
        import subprocess
        p = subprocess.Popen('git rev-list --count master', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        return p.stdout.readline().rstrip()
    else:
        print ("git not found")

def stripSymbols(env, toolchainType, buildTypeName, targetName, outputDirPath, outputExtension, strip_symbols):
    if(strip_symbols == "yes"):
        if(toolchainType == 'GCC'):
        	print("Stripping symbols from " + targetName + '.' + outputExtension)
        	env.AddPostAction(targetName + '.' + outputExtension, Action( env['STRIP'] + ' -s -g -S -d --strip-unneeded --strip-dwo -X -x $TARGET -o ' + outputDirPath + targetName + '.' + outputExtension ))
        else:
        	print("Stripping symbols from " + targetName + '.' + outputExtension + " not supported!")

def getToolchainObjFolder(toolchainName):
	if(toolchainName=="GCC"):
		return "aout"
	if(toolchainName=="GCCBROWNELF"):
		return "brownelf"
	return "unsupported"


# Set number of jobs, depending on number of cores
num_cpu = int(os.environ.get('NUMBER_OF_PROCESSORS', 2))
SetOption('num_jobs', num_cpu)

print("Running with %d job(s)." % GetOption('num_jobs')) 

env = Environment(ENV = os.environ, tools=['default'])

Help("Atari TOS gcc minimal startup\n \
Copyright 2011-2023 Mariusz Buras / Pawel Goralski\n \
Type: 'Set build variables in SConstruct'\n \
where name of 'targetPlatform' can be: \n \
        'F030' - for Atari Falcon030 / TT build\n \
        'ST' - for Atari ST build \n \
        'CT60' - for Atari Falcon CT60/63\n \
'lang' - program startup type ('C' - C language, 'CPP' - C++),\n \
'host' - computer host name,\n\
'toolchain' - toolchain type 'GCC' or 'GCCBROWNELF',\n\
'toolchain_c_prefix' - toolchain compiler name prefix,\n\
'toolchain_version' - compiler version string (x.x.x, eg. '4.3.4' or '7.3.1'),\n\
'toolchain_prefix' - toolchain prefix path (root of the toolchain folder),\n\
'toolchain_use_cross' - use cross compiler, set to 'y' by default\n\
'build_config' - build type ('debug', 'release'),\n\
'use_fpu' - enable/disable hardware fpu code generation (valid only on F030 builds),\n \
'enable_lto' - eneable link time optimisation. Only valid on gcc 6/7/8/9.x.x brownelf compiler.\n \
'enable_stack_protector' - enable stack protector. \n\
'enable_strip_symbols' - strip symbols. \n\
'debug_info_dri' - generate DRI/GST symbol table (useful for Hatari and native debuggers). \n\
'debug_info_gdb' - generate debug symbols. \n\
'debug_generate_symbol_map_file' - generate symbol map file. \n\
'use_custom_alloc' - use memory allocation functions provided by user \n \
'conout_enable' - disable / enable console output,\n \
'debug_level' - sets debug level, only valid in debug builds. Level can be set to (0..2),\n \
'redirect_to_serial' - redirect console output to serial port,\n \
")

Import(\
		'targetPlatform',\
		'lang',\
        'host',\
		'toolchain',\
		'toolchain_c_prefix',\
		'toolchain_version',\
		'toolchain_prefix',\
		'toolchain_use_cross',\
		'build_config',\
		'use_fpu',\
		'enable_lto',\
		'enable_stack_protector',\
		'enable_strip_symbols',\
		'debug_info_dri',\
		'debug_info_gdb',\
		'debug_generate_symbol_map_file',\
		'use_custom_alloc',\
		'conout_enable',\
		'debug_level',\
		'redirect_to_serial'\
		)

rootdir = Dir('../').abspath
projdir = Dir('#').abspath
outputdir = Dir('.').abspath

if((lang !="C" and lang!="CPP")):
	print('ERROR: Invalid language target ( lang ).\nCan be only "C" (for C language) or "CPP" (for C++ language).')
	exit(-1);

env['LIBSUFFIX'] ='.a'

PREFIX = toolchain_prefix
GCC_PREFIX = toolchain_c_prefix
GCC_VERSION_STRING = toolchain_version

# should we use libgcc or internal builtins, enabled ('yes') by default
LINK_LIBGCC = 'yes'

if(toolchain=='GCC'):
    env['PROGSUFFIX'] = '.tos'
elif(toolchain=='GCCBROWNELF'):
    env['PROGSUFFIX'] = '.elf'
else:
    print('ERROR: Undefined target toolchain. Exiting...')
    exit(-1)

if(lang=='CPP'):
	env['CXXPATH'] =  [PREFIX + '/' + GCC_PREFIX + '/include/c++/' + GCC_VERSION_STRING +'/' + PREFIX]
	env['CPPPATH'] = ['']
	env['LIBPATH'] = ['']
elif(lang=='C'):
	env['CXXPATH'] = ['']
	env['CPPPATH'] = ['']
	env['LIBPATH'] = ['']

if(PREFIX):
	env['CC'] = PREFIX + '/bin/' + GCC_PREFIX + '-gcc' + '-' + GCC_VERSION_STRING
	env['CXX'] = PREFIX + '/bin/' + GCC_PREFIX + '-c++'
	env['OBJCOPY'] = PREFIX + '/bin/' + GCC_PREFIX + '-objcopy'
	env['STRIP'] = PREFIX + '/bin/' + GCC_PREFIX + '-strip'
	env['STACK'] = PREFIX + '/bin/' + GCC_PREFIX + '-stack'
	env['AR'] = PREFIX + '/bin/' + GCC_PREFIX + '-ar'
	env['RANLIB'] = PREFIX + '/bin/' + GCC_PREFIX + '-ranlib'
else:	
	env['CC'] = GCC_PREFIX + '-gcc'
	env['CXX'] = GCC_PREFIX + '-c++'
	env['OBJCOPY'] = GCC_PREFIX + '-objcopy'
	env['STRIP'] = GCC_PREFIX + '-strip'
	env['STACK'] = GCC_PREFIX + '-stack'
	env['AR'] = GCC_PREFIX + '-ar'
	env['RANLIB'] = GCC_PREFIX + '-ranlib'

env['AS'] = 'vasmm68k_mot'            # AS override

# ST/e, F030, CT60 program flags
CFLAGS=''
LDFLAGS=''
VASM_FLAGS=''
CPPFLAGS=''
CXXFLAGS=''
CODEGEN = '-std=c17 -pipe'
CODEGEN_CPP = '-std=c++14 -pipe'

LIB_TARGET='not_set'
TARGET='-DTARGET_' + targetPlatform
VASM_INCLUDES = '-I' + projdir + '/include'

if(debug_info_gdb=='yes'):
    CODEGEN += ' -g -grecord-gcc-switches'
    CODEGEN_CPP += ' -g -grecord-gcc-switches'

if(enable_stack_protector=='yes'):
    CODEGEN += ' -fstack-protector-strong'
    CODEGEN_CPP += ' -fstack-protector-strong'
    CPPFLAGS += '-DENABLE_STACK_PROTECTOR=1 '
    CXXFLAGS += '-DENABLE_STACK_PROTECTOR=1 '
else:
	CPPFLAGS += '-DENABLE_STACK_PROTECTOR=0 '
	CXXFLAGS += '-DENABLE_STACK_PROTECTOR=0 '

if( ( (debug_info_dri=='yes') and (toolchain!='GCCBROWNELF')) ):
    print ("Generating DRI/GST symbol table.")
    LDFLAGS += '-Wl,--traditional-format '

STACK_SIZE=0
if(lang == "C"):
	STACK_SIZE=8*1024
elif (lang == "CPP"):
	STACK_SIZE=65*1024

LDFLAGS += '-nostdlib -nodefaultlibs -nolibc -nostartfiles -Wl,-e_start '
    
if(build_config == "debug"):
    CPPFLAGS+='-DDEBUG -fomit-frame-pointer '
    CXXFLAGS+='-DDEBUG -fomit-frame-pointer '
    VASM_FLAGS='-showcrit -quiet -spaces -no-opt -DSTACK_SIZE=' + str(STACK_SIZE) + ' '
else:
    CPPFLAGS+='-DNDEBUG -O2 -fomit-frame-pointer '
    CXXFLAGS+='-DNDEBUG -O2 -fomit-frame-pointer '
    VASM_FLAGS='-showcrit -quiet -spaces -DSTACK_SIZE=' + str(STACK_SIZE) + ' '

# enable/disable default output to serial port instead of console 
if(redirect_to_serial==1):
    print('Redirect output to serial port: ENABLED')
    VASM_FLAGS+='-DREDIRECT_OUTPUT_TO_SERIAL=1 '
else:
    print('Redirect output to serial port: DISABLED')
    VASM_FLAGS+='-DREDIRECT_OUTPUT_TO_SERIAL=0 '

if(use_fpu == 'yes'):
	if(targetPlatform != 'CT60'):
		CPPFLAGS += '-m68881 '
		CXXFLAGS += '-m68881 '
		VASM_FLAGS += '-m68881 '
else:
		CPPFLAGS += '-msoft-float '
		CXXFLAGS += '-msoft-float '
		VASM_FLAGS += '-no-fpu '

if(toolchain=='GCC'):
	CODEGEN_CPP += ' -fno-rtti -fno-exceptions -fno-unwind-tables -fleading-underscore -ffunction-sections -fdata-sections'

	if(targetPlatform == "ST"):
		CPPFLAGS += '-m68000 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68000 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68000 -L' + outputdir
		VASM_FLAGS += '-m68000 -Faout ' + VASM_INCLUDES
		LIB_TARGET = '000'
	elif(targetPlatform == "F030"):
		CPPFLAGS += '-m68020-60 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68020-60 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68020-60 -L' + outputdir
		VASM_FLAGS += '-m68020up -Faout ' + VASM_INCLUDES
		LIB_TARGET = '020-60'
	elif(targetPlatform == "CT60"):
		CPPFLAGS += '-m68060 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68060 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68060 -L' + outputdir
		VASM_FLAGS += '-m68060 -Faout ' + VASM_INCLUDES
		LIB_TARGET = '060'
	else:
		print('Unsupported target. Exiting...')
		exit(-1)
elif(toolchain == 'GCCBROWNELF'):
	CODEGEN += ' -fleading-underscore -ffunction-sections -fdata-sections -fleading-underscore -ffunction-sections -fdata-sections -fno-plt -fno-pic'
	CODEGEN_CPP += ' -fno-rtti -fno-exceptions -fno-unwind-tables -fleading-underscore -ffunction-sections -fdata-sections -fno-plt -fno-pic'
	
	if(targetPlatform == "ST"):
		CPPFLAGS += '-m68000 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68000 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68000 -Wl,--gc-sections -Wl,--emit-relocs -Ttext=0 -L' + outputdir 
		VASM_FLAGS += '-m68000 -Felf ' + VASM_INCLUDES
		LIB_TARGET = '000'
	elif(targetPlatform == "F030"):
		CPPFLAGS += '-m68020-60 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68020-60 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68020-60 -Wl,--gc-sections -Wl,--emit-relocs -Ttext=0 -L' + outputdir
		VASM_FLAGS += '-m68020up -Felf ' + VASM_INCLUDES
		LIB_TARGET = '020-60'
	elif(targetPlatform == "CT60"):
		CPPFLAGS += '-m68060 ' + CODEGEN + ' -I' + projdir + '/include '
		CXXFLAGS += '-m68060 ' + CODEGEN_CPP + ' -I' + projdir + '/include '
		LDFLAGS += '-m68060 -Wl,--gc-sections -Wl,--emit-relocs -Ttext=0 -L' + outputdir
		VASM_FLAGS += '-m68060 -Felf ' + VASM_INCLUDES
		LIB_TARGET = '060'
	else:
		print('Unsupported target. Exiting...')
		exit(-1)
else:
    print('ERROR: Undefined target toolchain. Exiting...')
    exit(-1)

env['CPPFLAGS'] = TARGET + ' ' + CPPFLAGS
env['CXXFLAGS'] = TARGET + ' ' + CXXFLAGS

# set environment
env['LINKFLAGS'] = LDFLAGS
env['ASFLAGS'] = VASM_FLAGS
env['VASM_FLAGS'] = VASM_FLAGS

if(LINK_LIBGCC == 'yes'):
	env['LINKFLAGS']+= ' -lgcc'

# startup
startup_src = 'not_set'
prgName = 'not_set'
additionalLinkerFlags = 'not_set'

# build startup
if(lang == "C"):
	if(build_config == "debug"):
		print('Building custom startup for C compiler variant [DEBUG] TARGET: ' + 'mc68' + LIB_TARGET)
	else:
		print('Building custom startup for C compiler variant [RELEASE] TARGET: ' + 'mc68' + LIB_TARGET)

	prgName = 'testc' + LIB_TARGET

	if(debug_generate_symbol_map_file=='yes'):
		additionalLinkerFlags=' -Wl,-Map=' + outputdir + '/' + 'startup' + LIB_TARGET + '.map '
	else:
		additionalLinkerFlags=''

	startupSource=''
	targetLibs=''

	if(LINK_LIBGCC == 'yes'):
		startupCSource = ['src/mStartup.c', 'src/mStartupAsmC.s', 'src/crthooks.c']
		targetLibs = ['startup' + LIB_TARGET, 'gcc']
	else:
		startupCSource = ['src/mStartup.c', 'src/mStartupAsmC.s', 'src/crthooks.c','src/builtin.c', 'src/builtin_m68k.s']
		targetLibs = ['startup' + LIB_TARGET]

	env.StaticLibrary(target = 'startup' + LIB_TARGET, source = startupCSource, LINKFLAGS = env['LINKFLAGS'] + additionalLinkerFlags) 

	if(debug_generate_symbol_map_file=='yes'):
		additionalLinkerFlags=' -Wl,-Map=' + outputdir + '/' + prgName + '.map '
	else:
		additionalLinkerFlags=''

	env.Program(target = (prgName + env['PROGSUFFIX']), source = ['testc/main.c'], LINKFLAGS = env['LINKFLAGS'] + additionalLinkerFlags, LIBS = targetLibs, PROGSUFFIX = env['PROGSUFFIX'])
	
	folderName = getToolchainObjFolder(toolchain)

	stripSymbols(env, toolchain, build_config, prgName, './build/' + folderName + '/c/' + build_config + '/' + 'mc68' + LIB_TARGET + '/', 'tos', enable_strip_symbols)
	convertElfToTos(env, toolchain_prefix, toolchain, build_config, prgName, './build/' + folderName + '/c/' + build_config + '/' + 'mc68' + LIB_TARGET + '/', 'tos', debug_info_dri)
	
	setFastRamFlags(env, toolchain, prgName)

# end C handling

if(lang == "CPP"):
	if(build_config == "debug"):
		print('Building custom startup for C++ compiler variant [DEBUG] TARGET: ' + 'mc68' + LIB_TARGET ) 
	else:
		print('Building custom startup for C++ compiler variant [RELEASE] TARGET: ' + 'mc68' + LIB_TARGET )

	# build test c++ program and link against our custom startup
	prgName = 'testcpp' + LIB_TARGET

	if(debug_generate_symbol_map_file=='yes'):
		additionalLinkerFlags=' -Wl,-Map=' + outputdir + '/' + 'startupCpp' + LIB_TARGET + '.map '
	else:
		additionalLinkerFlags=''

	targetLibs=''
	startupCppSource=''

	if(LINK_LIBGCC == 'yes'):
		startupCppSource = ['src/mStartup.c','src/mStartupAsmC.s', 'src/crthooks.c']
		targetLibs = ['startupCpp'+ LIB_TARGET]
	else:
		startupCppSource = ['src/mStartup.c','src/mStartupAsmC.s', 'src/crthooks.c', 'src/builtin.c', 'src/builtin_m68k.s']
		targetLibs = ['startupCpp'+ LIB_TARGET,'gcc']

	env.StaticLibrary(target = 'startupCpp' + LIB_TARGET, source = startupCppSource, LINKFLAGS = env['LINKFLAGS'] + additionalLinkerFlags) 

	if(debug_generate_symbol_map_file=='yes'):
		additionalLinkerFlags=' -Wl,-Map=' + outputdir + '/' + prgName + '.map '
	else:
		additionalLinkerFlags=''

	folderName = getToolchainObjFolder(toolchain)

	env.Program (target = prgName, source = ['testcpp/main.cpp'], LINKFLAGS = env['LINKFLAGS'] + additionalLinkerFlags, LIBS = targetLibs, PROGSUFFIX = env['PROGSUFFIX']);
	stripSymbols(env, toolchain, build_config, prgName, './build/' + folderName + '/cpp/' + build_config + '/' + 'mc68' + LIB_TARGET + '/', 'tos', enable_strip_symbols)
	convertElfToTos(env, toolchainPrefix, toolchain, build_config, prgName, './build/' + folderName + '/cpp/' + build_config + '/' + 'mc68' + LIB_TARGET + '/', 'tos', debug_info_dri)
	setFastRamFlags(env, toolchain, prgName)

# end CPP handling
