#!/bin/bash
# build c startup for all targets (gcc)

args=("$@") 
ARGS=${#args[@]} 

brownelf=0

for (( i=0;i<$ARGS;++i)); do 
if [ ${args[${i}]} = "--brownelf" ]; then
  brownelf=1
fi
done

SCONSTRUCT_PREFIX=SConstruct

if [ ${brownelf} -eq 1 ]; then
SCONSTRUCT_PREFIX=${SCONSTRUCT_PREFIX}_brownelf
fi

SCONSTRUCT_PREFIX=${SCONSTRUCT_PREFIX}_C++

CONFIGDIR=${PWD}/buildConfigs

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_000_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_000_debug.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_000_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_000_release.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_020-60_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_020-60_debug.scons  ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_020-60_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_020-60_release.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_060_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_060_debug.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_060_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_060_release.scons ./
