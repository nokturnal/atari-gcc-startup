#!/bin/bash
# build c startup for all targets

args=("$@") 
ARGS=${#args[@]} 

brownelf=0

for (( i=0;i<$ARGS;++i)); do 
if [ ${args[${i}]} = "--brownelf" ]; then
  brownelf=1
fi
done

SCONSTRUCT_PREFIX=SConstruct

if [ ${brownelf} -eq 1 ]; then
SCONSTRUCT_PREFIX=${SCONSTRUCT_PREFIX}_brownelf
fi

CONFIGDIR=${PWD}/buildConfigs

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_000_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_000_debug.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_000_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_000_release.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_020-60_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_020-60_debug.scons  ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_020-60_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_020-60_release.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_060_debug.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_060_debug.scons ./

scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_060_release.scons -c ./
scons --sconstruct=${CONFIGDIR}/${SCONSTRUCT_PREFIX}_C_060_release.scons ./