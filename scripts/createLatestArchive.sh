#!/bin/bash

args=("$@") 
ARGS=${#args[@]} 

LIB_FOLDER_NAME=
brownelf=0

for (( i=0;i<$ARGS;++i)); do 
if [ ${args[${i}]} = "--brownelf" ]; then
  brownelf=1
fi
done

if [ ${brownelf} -eq 1 ]; then
LIB_FOLDER_NAME='brownelf'
DEST_FOLDER_NAME=atari-gcc-brownelf-c-startup
else
LIB_FOLDER_NAME='aout'
DEST_FOLDER_NAME=atari-gcc-c-startup
fi

echo Archiving ${DEST_FOLDER_NAME} library variant

STARTUP_SRC_BUILD_ROOT=${PWD}/build
DEST_PATH=${PWD}/${DEST_FOLDER_NAME}
STARTUP_TARGET_ROOT=${DEST_PATH}/lib

rm -rf ${DEST_PATH}

mkdir ${DEST_PATH}
mkdir ${DEST_PATH}/include

echo Create dependencies folder structure

mkdir ${STARTUP_TARGET_ROOT}
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/

mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68000
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68020-60
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68060

mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68000
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68020-60
mkdir ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68060

cp -r ./include/ ${DEST_PATH}

cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/debug/mc68000/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68000/
cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/release/mc68000/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68000/
cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/debug/mc68020-60/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68020-60/
cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/release/mc68020-60/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68020-60/
cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/debug/mc68060/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/debug/mc68060/
cp ${STARTUP_SRC_BUILD_ROOT}/${LIB_FOLDER_NAME}/c/release/mc68060/*.a ${STARTUP_TARGET_ROOT}/${LIB_FOLDER_NAME}/release/mc68060/

echo Creating archive
tar -czvf ${DEST_PATH}_latest.tgz -C ${DEST_PATH} .
