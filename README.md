# Minimal GNU gcc startup for m68k Atari TOS computers (Atari (Mega)ST/e, TT030, Falcon030, CT60) #
This repository contains custom GCC startup for 16/32 bit Atari targets without standard library (libc) dependencies. 

Startup code is for 16/32 bit Atari developers writing programs / games / demos for Atari ST/ STe/ TT/ F030/ CT60, which consider executable sizes produced by GNU GCC too big. See article: [Reducing gcc executables ...](https://bus-error.nokturnal.pl/article12-Reducing-size-of-executables-produced-by-gcc)

### Compile from sources ###

#### Required tools ####

 *  [SCons build tool](http://scons.org/)
 *  GCC C cross compiler supporting C99 or better - [Thorsten Otto's FreeMint cross tools page](https://tho-otto.de/crossmint.php) and/or [gcc(brownelf)](https://bitbucket.org/ggnkua/bigbrownbuild-git/src/master)
 *  [VASM m68k cross compiler](http://sun.hasenbraten.de/vasm/)
 *  Environment like [Cygwin(x64_86)](https://www.cygwin.com/) under Windows or Linux.


1. Clone repository (**"git clone git@bitbucket.org:nokturnal/atari-gcc-startup.git"**) somewhere on your hard drive.

2. Adjust aout/brownelf your local compiler version configuration in **"./buildsystem/toolchain_gcc.scons"** and/or **"./buildsystem/toolchain_gcc_brownelf.scons"**.

3. Go to repository directory and type:

	*  Run **"./scripts/buildCStartup_gcc.sh"** for standard aout gcc build variant 
	*  Run **"./scripts/buildCStartup_brownelf.sh --brownelf"** for gcc brownelf build for C startup 
	*  Run **"./scripts/buildC++Startup_gcc.sh"** for standard aout gcc build 
	*  Run **"./scripts/buildC++Startup_brownelf_gcc.sh --brownelf"** for gcc brownelf build for C++ startup.
	*  Alternatively you can run **"./scripts/makeAll.sh"** to build both variants and create distribution archives.


Static libraries startupXXX or startupCppXXX (with "a" extension) will be build in: build\OBJFORMAT\LANG\BUILD_TYPE\TARGET_CPU


Where:

 *  OBJFORMAT can be **'aout'** or **'brownelf'**
 *  LANG can be **"c"** or **"cpp"**
 *  BUILD_TYPE = **debug** or **release**
 *  TARGET_CPU = **mc68000**, **mc68020-40**, **mc68060**

Some additional parameters like 'STACK_SIZE' and 'redirect_to_serial' can be set in SConscript / SConstruct. By default stack size for C is set to 16kb, 64kb for c++. Serial port output redirection is turned off by default.


## Link output files with your program ##
Link preferred version startup (startupXXX or startupCppXXX) from chosen folder to your program with "-nostdlib" and "-nostartfiles" switch. Startup object files need to be first. 

Enjoy small program sizes with modern compilers :).

## Notes
By default linking libgcc will be needed for builtin functions (float / integer operations) used by gcc. This can be overriden in SConscript file, by setting LINK_LIBGCC to 'no'. In this case some missing symbols will be provided, but still this parts remain unfinished and not very well tested. Repository provides only minimal startup code with several gcc builtin functions needed by compiler to get going. Also C++ part isn't very well maintained and tested, but should provide good starting point. Any contributions are welcome.

Startup code is only one part, you will also need to implement alot of other functions depending on personal needs (console input / output, string handling, memory management, filesystem handling and so on). If you don't want to do this work, then check out [libcmini library](https://github.com/freemint/libcmini), which provides minimal libc implementation. 
